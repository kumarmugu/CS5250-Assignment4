'''
CS5250 Assignment 4, Scheduling policies simulator
Author: Minh Ho, Mugunthan
Input file:
    input.txt
Output files:
    FCFS.txt
    RR.txt
    SRTF.txt
    SJF.txt

Apr 10th Revision 1:
    Update FCFS implementation, fixed the bug when there are idle time slices between processes
    Thanks Huang Lung-Chen for pointing out
Revision 2:
    Change requirement for future_prediction SRTF => future_prediction shortest job first(SJF), the simpler non-preemptive version.
    Let initial guess = 5 time units.
    Thanks Lee Wei Ping for trying and pointing out the difficulty & ambiguity with future_prediction SRTF.
Remaining revision can be found in https://gitlab.com/kumarmugu/CS5250-Assignment4/commits/master/simulator.py
'''

import sys
from Queue import *
import copy

input_file = 'input.txt'

class Process:
    last_scheduled_time = 0
    def __init__(self, id, arrive_time, burst_time, remaining_time):
        self.id = id
        self.arrive_time = arrive_time
        self.burst_time = burst_time
        self.remaining_time = remaining_time
    #for printing purpose
    def __repr__(self):
        return ('[id %d : arrive_time %d,  burst_time %d,  remaining_time %d]'%(self.id, self.arrive_time, self.burst_time, self.remaining_time))

def FCFS_scheduling(process_list):
    #store the (switching time, proccess_id) pair
    schedule = []
    current_time = 0
    waiting_time = 0
    for process in process_list:
        if(current_time < process.arrive_time):
            current_time = process.arrive_time
        schedule.append((current_time,process.id))
        waiting_time = waiting_time + (current_time - process.arrive_time)
        current_time = current_time + process.burst_time
    average_waiting_time = waiting_time/float(len(process_list))
    return schedule, average_waiting_time

#Input: process_list, time_quantum (Positive Integer)
#Output_1 : Schedule list contains pairs of (time_stamp, proccess_id) indicating the time switching to that proccess_id
#Output_2 : Average Waiting Time
def RR_scheduling(process_list, time_quantum ):
    rr_process_list = copy.deepcopy(process_list)
    schedule = []
    no_of_process = float(len(rr_process_list))
    current_time = 0
    waiting_time = 0
    queue = Queue()
    previous_process_id = -1;

    #check for arrival initilly. Assume that at lease one process is there with arrival time 0
    i = 0
    for p in rr_process_list[:]:
        if(p.arrive_time <= current_time):
            queue.put(p)
            rr_process_list.pop(i)
            i -= 1
        i += 1

    #process until rr_process_list and queue become empty
    while (len(rr_process_list) != 0 or queue.qsize() != 0):
        quantum = time_quantum
        if(not queue.empty()):
            process = queue.get(0)

            #If the current process remaining_time is less than default time_quantum
            if(process.remaining_time < time_quantum):
                quantum = process.remaining_time
            
            #Not printing if the current executing process is same as previous
            if(previous_process_id != process.id):
                schedule.append((current_time, process.id))
            previous_process_id = process.id

            #update remaining_time
            process.remaining_time = process.remaining_time - quantum


            #increse the current time before check the new arrival process below
            current_time = current_time + quantum

            #update waiting_time for all remaining process in queue
            if(process.remaining_time == 0):
                waiting_time = waiting_time + (current_time - process.arrive_time - process.burst_time)

            #check and update the queue with new arrival process
            i = 0
            for p in rr_process_list[:]:
                if(p.arrive_time <= current_time):
                    queue.put(p)
                    rr_process_list.pop(i)
                    i -= 1
                i += 1

            #put current process to end of the queue if it require some more time
            if(process.remaining_time != 0):
                queue.put(process)
            
        else:
            #increase the current_time if no process in the queue
            current_time = rr_process_list[0].arrive_time

            #check and update the queue with new arrival process
            i = 0
            for p in rr_process_list[:]:
                if(p.arrive_time <= current_time):
                    queue.put(p)
                    rr_process_list.pop(i)
                    i -= 1
                i += 1

    #calculation of average_waiting_time
    average_waiting_time = waiting_time/no_of_process
    return schedule, average_waiting_time

def SRTF_scheduling(process_list):
    srtf_process_list = copy.deepcopy(process_list)
    schedule = []
    no_of_process = float(len(srtf_process_list))
    current_time = 0
    waiting_time = 0
    arrival_time_list = []

    #prepare the arrival_time_list for all the process and sort
    for process in srtf_process_list:
        arrival_time_list.append(process.arrive_time)
    arrival_time_list.sort();

    #Assume first process as initial process and remove the initial arrive_time from arrival_time_list
    execution_process = srtf_process_list[0]    
    arrival_time_list.remove(execution_process.arrive_time)

    #identify the next_arrival_time
    if(len(arrival_time_list) != 0):
        next_arrival_time = arrival_time_list[0]

    schedule.append((current_time, execution_process.id))
    previous_process_id = execution_process.id;

    #process until srtf_process_list become empty
    while (len(srtf_process_list) != 0):
        #if the process remaining_time finish before the next_arrival_time
        if(next_arrival_time == -1 or (current_time + execution_process.remaining_time <= next_arrival_time)):
            srtf_process_list.remove(execution_process)

            #increase the current_time
            current_time = current_time + execution_process.remaining_time

            #calculate waiting time
            waiting_time = waiting_time + (current_time - execution_process.arrive_time - execution_process.burst_time)

            execution_process.remaining_time = 0

            #Remove the current executable_process. Then identify new execution_process.
            for x in srtf_process_list:
                if((execution_process.remaining_time == 0 or x.remaining_time < execution_process.remaining_time) and x.arrive_time <= current_time):
                    execution_process = x

            #increment the current time as next_arrival_time if there is no process to run at this stage
            if(execution_process.remaining_time == 0):
                if(next_arrival_time != -1):
                    current_time = next_arrival_time
                else:
                    break

                #For new current time, identify new execution_process again
                for x in srtf_process_list:
                    if((execution_process.remaining_time == 0 or x.remaining_time < execution_process.remaining_time) and x.arrive_time <= current_time):
                        execution_process = x

            #remove the current_time if it is in the arrival_time_list
            if current_time in arrival_time_list:
                arrival_time_list.remove(current_time)

            ##update the next_arrival_time variable for next iteration
            if(len(arrival_time_list)!= 0):
                next_arrival_time = arrival_time_list[0]
        else: #if the current process cannot finish the execution before next_arrival_time

            #update the remaining_time fro current process
            execution_process.remaining_time = execution_process.remaining_time - (next_arrival_time - current_time)

            #update current_time with next_arrival_time
            current_time = next_arrival_time

            #For new current time, identify new execution_process again
            for x in srtf_process_list:
                if(x.remaining_time < execution_process.remaining_time and x.arrive_time <= current_time):
                    execution_process = x

            #remove the current_time if it is in the arrival_time_list
            if current_time in arrival_time_list:
                arrival_time_list.remove(current_time)

            #update the next_arrival_time
            if(len(arrival_time_list) != 0):
                next_arrival_time = arrival_time_list[0]
            else:
                next_arrival_time = -1

        #Not printing if the current executing process is same as previous
        if(previous_process_id != execution_process.id):
            schedule.append((current_time, execution_process.id))

        #update the previous process variable for next iteration
        previous_process_id = execution_process.id

    #calculation of average_waiting_time
    average_waiting_time = waiting_time/no_of_process
    return schedule, average_waiting_time

def SJF_scheduling(process_list, alpha):
    sjf_process_list = copy.deepcopy(process_list)
    schedule = []
    no_of_process = float(len(sjf_process_list))
    current_time = 0
    waiting_time = 0
    defaultPredictedTime = 5
    previous_process_id = -1
    predicted_times = {}
    actual_times = {}


    #Assume first process as initial process
    execution_process = sjf_process_list[0]
    predicted_times[execution_process.id] = defaultPredictedTime

    #process until sjf_process_list become empty
    while (len(sjf_process_list) != 0):

        #Not printing if the current executing process is same as previous
        if(previous_process_id != execution_process.id):
            schedule.append((current_time, execution_process.id))
        previous_process_id = execution_process.id

        sjf_process_list.remove(execution_process)

        #increase the current_time
        current_time = current_time + execution_process.burst_time
        actual_times[execution_process.id] = execution_process.burst_time
        predicted_time = alpha * actual_times[execution_process.id] + (1-alpha)*predicted_times[execution_process.id]
        predicted_times[execution_process.id] = predicted_time
        execution_process.remaining_time = 0

        #calculate waiting time
        waiting_time = waiting_time + (current_time - execution_process.arrive_time - execution_process.burst_time)

        #For new current time, identify new execution_process        
        for x in sjf_process_list:
            if x.id not in predicted_times:
                predicted_times[x.id] = defaultPredictedTime
            if((execution_process.remaining_time == 0 or predicted_times[x.id] < predicted_times[execution_process.id]) and x.arrive_time <= current_time):
                execution_process = x

        #increment the current time as next_arrival_time if there is no process to run at this stage
        if(execution_process.remaining_time == 0):
            if(len(sjf_process_list)!= 0):
                current_time = sjf_process_list[0].arrive_time

            #For new current time, identify new execution_process again
            for x in sjf_process_list:
                if((execution_process.remaining_time == 0 or predicted_times[x.id] < predicted_times[execution_process.id]) and x.arrive_time <= current_time):
                    execution_process = x

    #calculation of average_waiting_time
    average_waiting_time = waiting_time/no_of_process
    return schedule, average_waiting_time

def read_input():
    result = []
    with open(input_file) as f:
        for line in f:
            array = line.split()
            if (len(array)!= 3):
                print ("wrong input format")
                exit()
            result.append(Process(int(array[0]),int(array[1]),int(array[2]), int(array[2])))
    return result
def write_output(file_name, schedule, avg_waiting_time):
    with open(file_name,'w') as f:
        for item in schedule:
            f.write(str(item) + '\n')
        f.write('average waiting time %.2f \n'%(avg_waiting_time))


def main(argv):
    process_list = read_input()
    print ("printing input ----")
    for process in process_list:
        print (process)
    print ("simulating FCFS ----")
    FCFS_schedule, FCFS_avg_waiting_time =  FCFS_scheduling(process_list)
    write_output('FCFS.txt', FCFS_schedule, FCFS_avg_waiting_time )
    print ("simulating RR ----")
    RR_schedule, RR_avg_waiting_time =  RR_scheduling(process_list,time_quantum = 10)
    write_output('RR.txt', RR_schedule, RR_avg_waiting_time )
    print ("simulating SRTF ----")
    SRTF_schedule, SRTF_avg_waiting_time =  SRTF_scheduling(process_list)
    write_output('SRTF.txt', SRTF_schedule, SRTF_avg_waiting_time )
    print ("simulating SJF ----")
    SJF_schedule, SJF_avg_waiting_time =  SJF_scheduling(process_list, alpha = 0)
    write_output('SJF.txt', SJF_schedule, SJF_avg_waiting_time )

if __name__ == '__main__':
    main(sys.argv[1:])
